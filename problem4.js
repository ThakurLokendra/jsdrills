function getYear(inventory) {
    const array = [];
    if (inventory) {
        for (let i = 0; i < inventory.length; i++) {
            array.push(inventory[i].car_year);
        }
        return array;
    } else {
        return array;
    }
}

module.exports = getYear;