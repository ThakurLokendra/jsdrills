function getLastCar(inventory) {
    let max = Number.MIN_VALUE;
    let value;
    if (inventory) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].car_year > max) {
                max = inventory[i].car_year;
                value = inventory[i];
            }
        }
    } else {
        return [];
    }
    return value;
}

module.exports = getLastCar;