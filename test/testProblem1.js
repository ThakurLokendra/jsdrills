const inventory = require('/home/lokendra/js/inventory.js');
const problem1 = require('/home/lokendra/js/problem1.js');

const result = problem1(inventory, 33);

if (result.length == 0) {
    console.log(result);
} else {
    console.log("Car " + result.id + " is a " + result.car_year + " " + result.car_make + " " + result.car_model);
}