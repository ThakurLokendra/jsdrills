const inventory = require('/home/lokendra/js/inventory.js');
const problem2 = require('/home/lokendra/js/problem2.js');

const result = problem2(inventory);

if (result.length == 0) {
    console.log(result);
} else {
    console.log("Last car is a " + result.car_make + " " + result.car_model);
}