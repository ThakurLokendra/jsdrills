function sort(inventory) {
    if (inventory) {
        inventory.sort((a, b) => {
            if (a.car_model.toLowerCase() > b.car_model.toLowerCase()) {
                return 1;
            } else {
                return -1;
            }
        });
        return inventory;
    } else {
        return [];
    }
}

module.exports = sort;