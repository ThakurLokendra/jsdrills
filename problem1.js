function SearchData(inventory, searchId) {
    if (inventory && searchId) {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id === searchId) {
                return inventory[i];
            }
        }
        return [];
    } else {
        return [];
    }
}

module.exports = SearchData;